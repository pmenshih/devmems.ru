import { useAuthStore } from 'stores/auth.store'

// Обертка для отправки запросов на сервер через fetch.
export const apiRequest = async (url, method = 'GET', data) => {
  const token = useAuthStore().authToken
  const pars = {
    body: data ? JSON.stringify(data) : null,
    headers: {
      'Authorization': 'Bearer ' + token ? token : '',
      'Content-Type': 'application/json'
    },
    method: method,
  }
  return await fetch(url, pars)
  .then(response => response.json())
  .then(data => data)
}
