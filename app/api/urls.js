/*
 * Скрипт для объединения всех backend-маршрутов в единую сборку.
 * Впоследствии используется в мидлваре api.js.
 */

import express from 'express'

import { authPrefix, authRouter } from 'api/v1/auth'
import { libraryPrefix, libraryRouter } from 'api/v1/library'

const apiPrefix = '/api'
const apiRouter = express.Router()

apiRouter.use(authPrefix, authRouter)
apiRouter.use(libraryPrefix, libraryRouter)

export { apiPrefix, apiRouter }
