import express from 'express'

import { isAdmin } from 'api/v1/auth'

import {
  createArticle,
  getArticleById,
  loadLibrary,
  updateArticle,
} from 'app/dll/library'

const libraryPrefix = '/library'
const libraryRouter = express.Router()

libraryRouter.get(
  '/',
  async (_, res) => {
    const articles = await loadLibrary(isAdmin)
    return res.json(articles)
  }
)

libraryRouter.get(
  '/article/:articleId',
  async (req, res) => {
    const article = await getArticleById(req.params.articleId)
    return res.json(article)
  }
)

libraryRouter.patch(
  '/article/:articleId',
  async (req, res) => {
    if (isAdmin) await updateArticle(req.body)
    return res.json({})
  }
)

libraryRouter.post(
  '/article',
  async (req, res) => {
    if (isAdmin) await createArticle(req.body)
    return res.json({})
  }
)

export { libraryPrefix, libraryRouter }
