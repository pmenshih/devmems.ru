import { pbkdf2Sync } from 'crypto'
import { config as dotenv } from 'dotenv'
import express from 'express'
import { jwtVerify, SignJWT } from 'jose'

import { prisma } from 'dll/db'

dotenv()

const authPrefix = '/auth'
const authRouter = express.Router()

const checkToken = async (token) => {
  try {
    const { payload, _ } = await jwtVerify(
      token,
      Buffer.from(process.env.JWT_SECRET, 'utf8'),
      { audience: process.env.APP_NAME, issuer: process.env.APP_NAME }
    )
    return { isTokenValid: true, payload: payload }
  }
  catch (error) {
    return { isTokenValid: false, payload: null }
  }
}

const createJWT = async (payload) => new SignJWT(payload)
  .setProtectedHeader({ alg: process.env.JWT_ALGORITHM })
  .setIssuer(process.env.APP_NAME)
  .setAudience(process.env.APP_NAME)
  .setExpirationTime(process.env.JWT_LIFETIME)
  .sign(Buffer.from(process.env.JWT_SECRET, 'utf8'))

const getExpirationTime = (token) => {
  const date = new Date(token.exp * 1000)
  const datevalues = [
    date.getFullYear(),
    date.getMonth() + 1,
    date.getDate(),
    date.getHours(),
    date.getMinutes(),
    date.getSeconds(),
  ]
  return datevalues
}

const isAdmin = async (req, res, next) => {
  const isTokenValid = await checkToken(req.headers.authorization)
  if (!isTokenValid) return res.json({ error: 'Неверный токен' })
  else { next() }
}

authRouter.post(
  '/check_token',
  async (req, res) => {
    // Проверка токена.
    const { isTokenValid, payload } = await checkToken(
      req.headers.authorization
    )
    if (!isTokenValid) return res.json({ error: 'Неверный токен' })

    // Формирование нового токена с обновленой датой истечения.
    const newToken = await createJWT(payload)
    return res.json({ token: newToken })
})

authRouter.post(
  '/login',
  async (req, res) => {
    // Получение пользователя из БД.
    const user = await prisma.users.findUnique({
      where: { email: req.body.email }
    })
    if (!user) {
      res.json({ error: 'Пользователь не найден' })
      return
    }

    // Формирование хеша пароля.
    const passwordHash = pbkdf2Sync(
      req.body.password,
      // TODO: в серьезных проектах нужно использовать соль,
      // отличную от секретного ключа.
      process.env.JWT_SECRET, 100000, 64, 'sha512'
    ).toString('hex')

    // Проверка хеша пароля.
    if (user.hashed_password !== passwordHash) {
      return res.json({ error: 'Неверный пароль' })
    }
    else {
      // Формирование JWT.
      const token = await createJWT({ email: req.body.email })
      return res.json({ token: token })
    }
})

export { authPrefix, authRouter, isAdmin }
