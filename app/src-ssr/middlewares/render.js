import { ssrMiddleware } from 'quasar/wrappers'

export default ssrMiddleware(({ app, resolve, render, serve }) => {
  app.get(resolve.urlPath('*'), (req, res) => {
    res.setHeader('Content-Type', 'text/html')

    render(/* the ssrContext: */ { req, res })
    .then(html => { res.send(html) })
    .catch(err => {
      if (err.url) {
        if (err.code) {
          res.redirect(err.code, err.url)
        } else { res.redirect(err.url) }
      } else if (err.code === 404) {
        res.status(404).sendFile(resolve.public('/assets/404.html'))
      } else if (process.env.DEV) {
        // well, we treat any other code as error;
        // if we're in dev mode, then we can use Quasar CLI
        // to display a nice error page that contains the stack
        // and other useful information
        // serve.error is available on dev only
        serve.error({ err, req, res })
      } else {
        res.status(500).send('500 | Internal Server Error')
      }
    })
  })
})
