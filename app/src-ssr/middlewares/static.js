import { ssrMiddleware } from 'quasar/wrappers'

export default ssrMiddleware(({ app, resolve, render, serve }) => {
  app.get('/sitemap.xml', async (req, res) => {
    res.sendFile(resolve.public('assets/sitemap.xml'))
  })

  app.get('/robots.txt', async (req, res) => {
    res.sendFile(resolve.public('assets/robots.txt'))
  })
})
