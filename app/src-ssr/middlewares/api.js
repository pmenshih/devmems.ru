import { ssrMiddleware } from 'quasar/wrappers'

import { apiPrefix, apiRouter } from 'api/urls'

export default ssrMiddleware(({ app, resolve, render, serve }) => {
  app.use(apiPrefix, apiRouter)
})
