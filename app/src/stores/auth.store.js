import { defineStore } from 'pinia'

export const useAuthStore = defineStore('auth', {
  actions: {
    // Обертка для отправки запроса на сервер.
    async apiRequest(url, data) {
      const pars = {
        body: data ? JSON.stringify(data) : null,
        headers: {
          'Authorization': 'Bearer ' + this.authToken ? this.authToken : '',
          'Content-Type': 'application/json'
        },
        method: 'POST',
      }
      return await fetch('/api/auth' + url, pars)
      .then(response => response.json()).then((data) => data)
    },
    // Проверка токена на сервере.
    async isAuth() {
      // Если токена нет, то дальше проверять нечего.
      if (!this.authToken) return false

      return await this.apiRequest('/check_token', { token: this.authToken })
      .then((data) => {
        if (!data.token) return false
        // Обновление токена с актуальной датой истечения.
        localStorage.setItem('authToken', data.token)
        return true
      })
    },
    // Аутентификация.
    async login(email, password) {
      return await this.apiRequest('/login',
        { email: email, password: password }
      )
      .then((data) => {
        if (!data.token) return false
        localStorage.setItem('authToken', data.token)
        // ВАЖНО!
        // В этом месте делается намеренный жесткий редирект, потому как
        // сразу после авторизации через геттер authToken не видно
        // нового токена.
        location.href = this.returnUrl || '/'
        return true
      })
    },
    // Логаут.
    logout(redirect = true) {
      localStorage.removeItem('authToken')
      if (redirect) this.router.push({ name: 'main' })
    }
  },
  getters: {
    authToken() {
      // Проверка на выполнение на сервере для случая SSR.
      return process.env.SERVER ? false : localStorage.getItem('authToken')
    }
  },
  state: () => ({
    authDestination: null,
    enablePreloader: false,
    returnUrl: null,
  })
})
