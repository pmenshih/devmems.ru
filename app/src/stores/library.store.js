import { defineStore } from 'pinia'

import { apiRequest } from '../../api/request'

const apiPrefix = '/api/library'

export const useLibraryStore = defineStore('core', {
  actions: {
    async createArticle(article) {
      return await apiRequest(`${apiPrefix}/article`, 'POST', article)
    },
    async loadArticleById(articleId) {
      return await apiRequest(`${apiPrefix}/article/` + articleId)
      .then(data => {
        this.articles[articleId] = data
        return data
      })
    },
    async loadLibrary() {
      return await apiRequest(`${apiPrefix}`)
      .then(data => { this.library = data })
    },
    strftime(date, format = '%Y-%m-%d') {
      date = new Date(date)
      const pad = (nPad, n) => n.toString().padStart(nPad, '0')

      const map = {
        '%Y': pad(4, date.getFullYear()),
        '%m': pad(2, date.getMonth() + 1),
        '%d': pad(2, date.getDate()),
        '%H': pad(2, date.getHours()),
        '%M': pad(2, date.getMinutes()),
        '%S': pad(2, date.getSeconds()),
        '%z':
          (date.getTimezoneOffset() < 0 ? '+' : '-') +
          pad(4, Math.abs(date.getTimezoneOffset()))
      }

      return Object.entries(map).reduce(
        (prev, [key, value]) => prev.replaceAll(key, value),
        format
      )
    },
    async updateArticle(article) {
      await apiRequest(
        `${apiPrefix}/article/` + article.id,
        'PATCH',
        article
      )
    },
  },
  state: () => ({
    articles: {},
    library: [],
  })
})
