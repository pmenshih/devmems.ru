import {
  adminRoutes as libraryAdmin,
  routes as libraryRoutes
} from 'src/router/library_router'

const AdminIndex = () => import('src/pages/AdminIndex.vue')
const AdminLayout = () => import('src/layouts/AdminLayout.vue')
const AdminLogin = () => import('src/pages/AdminLogin.vue')
const MainLayout = () => import('src/layouts/MainLayout.vue')
const PreloaderPage = () => import('src/pages/PreloaderPage.vue')

const routes = [
  // Роуты сайта.
  {
    children: [
      libraryRoutes,
    ],
    component: MainLayout,
    name: 'main',
    path: '/',
    redirect: { name: 'library' }
  },
  {
    component: PreloaderPage,
    name: 'preloader',
    path: '/preloader'
  },

  // Логин в админку идет отдельным роутом,
  // чтобы не было проблем с бесконечным редиректом.
  {
    component: AdminLogin,
    meta: { hideForAuth: true },
    name: 'admin/login',
    path: '/admin/login',
  },
  // Роуты админки.
  {
    children: [
      {
        component: AdminIndex,
        name: 'admin/index',
        path: '',
      },
      libraryAdmin,
    ],
    component: AdminLayout,
    meta: { requiresAuth: true },
    name: 'admin',
    path: '/admin',
  },
]

export default routes
