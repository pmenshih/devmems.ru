const LibraryAdmin = () => import('src/pages/LibraryAdmin.vue')
const LibraryArticle = () => import('src/pages/LibraryArticle.vue')
const LibraryIndex = () => import('src/pages/LibraryIndex.vue')

const adminRoutes = {
  component: LibraryAdmin,
  name: 'admin/library',
  path: 'library',
}

const routes = {
  children: [
    {
      component: LibraryIndex,
      name: 'library',
      path: ''
    },
    {
      component: LibraryArticle,
      name: 'library/article',
      path: 'article/:articleId'
    }
  ],
  path: 'library',
}

export { adminRoutes, routes }
