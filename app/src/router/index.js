import { route } from 'quasar/wrappers'
import { createRouter, createMemoryHistory, createWebHistory, createWebHashHistory } from 'vue-router'
import routes from './main_router'

import { useAuthStore } from 'stores/auth.store'

export default route(() => {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : (process.env.VUE_ROUTER_MODE === 'history' ? createWebHistory : createWebHashHistory)

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    history: createHistory(process.env.VUE_ROUTER_BASE)
  })

  Router.beforeEach(async (to, from) => {
    const authStore = useAuthStore()

    // Проверка флагов авторизации.
    //   - hideForAuth: скрыто для авторизованных, редирект на главную;
    //   - requiresAuth: только авторизованным;
    const hideForAuth = to.matched.some(r => r.meta.hideForAuth)
    const requiresAuth = to.matched.some(r => r.meta.requiresAuth)

    // Для случая SSR не проверяем авторизацию и редиректы.
    if (process.env.SERVER) {
      // Отработка случая запроса страницы, которая требует авторизации.
      // Так как на уровне сервера токена нет, то, если страница требует
      // авторизации, мы сохраняем адрес назначения и отдаем прелоадер.
      // А прелоадер, так как он будет окончательно инициализирован уже на
      // стороне клиента, разберирается сам, куда — на запрошенную страницу
      // или страницу логина — отправлять пользователя.
      if (requiresAuth) {
        authStore.enablePreloader = true
        authStore.authDestination = to.fullPath
        return { name: 'preloader' }
      }
      return true
    }

    // Если прелоадер включен, то его надо выключить,
    // чтобы не зацикливать его собственную загрузку.
    if (authStore.enablePreloader) {
      authStore.enablePreloader = false
      return { name: 'preloader' }
    }
    
    if (!hideForAuth && !requiresAuth) return true

    const isAuth = await authStore.isAuth()
    if (requiresAuth && !isAuth) {
      authStore.returnUrl = to.fullPath
      return { name: 'admin/login' }
    }
    if (hideForAuth && isAuth) {
      return { name: 'main' }
    }
  })

  return Router
})
