/*
 * Скрипт для генерации хеша пароля.
 */

const { pbkdf2Sync } = require('crypto')
require('dotenv').config({ path: '../.env' })

// Пароль передаётся в качестве аргумента командной строки.
const password = process.argv[2]
const passwordHash = pbkdf2Sync(
  password,
  // TODO: в серьезных проектах нужно использовать соль,
  // отличную от секретного ключа.
  process.env.JWT_SECRET,
  100000,
  64,
  'sha512'
).toString('hex')
console.log('Password hash:', passwordHash)
