/*
 * Создание и экспорт клиента БД.
 * Создано по мотивам документации: https://www.prisma.io/docs/guides/performance-and-optimization/connection-management#prismaclient-in-long-running-applications
 */

import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

export { prisma }
