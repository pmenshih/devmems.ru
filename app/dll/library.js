/*
 * Запросы к БД для работы с библиотекой.
 */

import { prisma } from './db'
import { config as dotenv } from 'dotenv'
import fs from 'fs'

dotenv()

const createArticle = async (article) => {
  const pars = {
    data: {
      body: article.body,
      date_create: new Date(),
      descr: article.descr,
      published: article.published,
      title: article.title,
    },
  }

  await prisma.library_articles.create(pars)
  await generateSitemap()
  return true
}
const generateSitemap = async () => {
  /**
   * Генерация sitemap.xml для всего сайта
   * после изменения или добавления статьи в библиотеке.
   * TODO: место этой функции должно быть не в этом модуле.
   * */ 
  let sitemap = '<?xml version="1.0" encoding="UTF-8"?>\n'
  sitemap += '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n'
  // Страница библиотеки.
  sitemap += `<url>
    <loc>https://${process.env.APP_NAME}/library</loc>
    <changefreq>weekly</changefreq>
    <priority>1.0</priority>
  </url>\n`
  // Статьи библиотеки.
  const articles = await prisma.library_articles.findMany({
    select: { id: true },
    where: { published: true },
  })
  articles.forEach((article) => {
    sitemap += `  <url>
    <loc>https://${process.env.APP_NAME}/library/article/${article.id}</loc>
    <changefreq>monthly</changefreq>
    <priority>0.5</priority>
  </url>\n`
  })
  // Закрываем urlset.
  sitemap += '</urlset>'
  // Сохраняем sitemap.xml в файл.
  fs.writeFileSync('assets/sitemap.xml', sitemap)
  return true
}
const getArticleById = async (articleId, isAdmin = false) => {
  const pars = {
    where: {
      id: { equals: Number(articleId) },
    }
  }
  // Для публичной части API не показываем неопубликованные статьи.
  if (!isAdmin) pars.where.published = { equals: true }

  return await prisma.library_articles.findFirst(pars)
}
const loadLibrary = async (isAdmin) => {
  const pars = { orderBy: { date_create: 'desc' } }
  if (!isAdmin) {
    pars.select = {
      date_create: true,
      descr: true,
      id: true,
      title: true,
    }
    pars.where = { published: true }
  }

  return await prisma.library_articles.findMany(pars)
}
const updateArticle = async (article) => {
  const pars = {
    data: {
      body: article.body,
      descr: article.descr,
      published: article.published,
      title: article.title,
    },
    where: { id: article.id },
  }

  await prisma.library_articles.update(pars)
  await generateSitemap()
  return true
}

export {
  createArticle,
  getArticleById,
  loadLibrary,
  updateArticle,
}
