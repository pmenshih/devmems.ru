# SSR/PWA приложение
В папке содержится все нужное для сборки SSR/PWA приложения.

## Структура файлов и папок
### Папки:
- [api](./api/) — логика backend API для общения с фронтом;
- [assets](./assets/) — вспомогательные самодельные утилиты;
- [dll](./dll/) (**D**ata **L**ogic **L**ibrary) — все необходимое для работы с БД: схема Prisma, клиент, а также интерфейсы для работы с моделями;
- [public](./public/) — папка со статичными ассетами сайта: картинки, фавики, robots.txt, sitemap.xml и тому подобное;
- [src](./src/) — Vue исходники сайта;
- [src-pwa](./src-pwa/) — автосгенерированные Quasar'ом файлы для PWA-версии приложения;
- [src-ssr](./src-ssr/) — серверная логика, построенная на Express.

### Файлы:
- [.env.sample](./.env.sample) — шаблон .env-файла приложения;
- [.eslintignore](./.eslintignore) + [.eslintrc.js](./.eslintrc.js) — конфигурационные файлы eslint (будь он неладен);
- [index.html](./index.html) — корневой файл PWA-приложения, точка входа Quasar;
- [jsconfig.json](./jsconfig.json) — [конфигурационный файл для TS](https://code.visualstudio.com/docs/languages/jsconfig);
- [package.json](./package.json) — зависимости и настройки Ноды;
- [postcss.config.js](./postcss.config.js) — конфиг PostCSS;
- [quasar.config.js](./quasar.config.js) — конфиг Quasar'а.